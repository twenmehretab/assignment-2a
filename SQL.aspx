﻿<%@ Page Language="C#" MasterPageFile="~/site.master" Inherits="assignment2a_N01294019.Default" %>

<asp:Content runat="server" ContentPlaceHolderID="Concept">
    <p>An SQL Concept:</p>
    <ul>
      <li>"joins"</li>
        <br>
      <li>my understanding of a join is a way to link to different tables</li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="myCode">
   <p>My SQL Code:</p>
    <ul>
      <li>an example of a join I created is: <br>
            select invoices.invoice_description, invoices.invoice_amount, clients.clientphone,
            clients.clientemail, clients.clientfname, clients.clientlname, clients.clientid, <br>
            from invoices 
            left join clients 
            on invoices.clientid = clients.clientid <br>
            where invoices.invoice_amount >1000 <br>
            order by clients.clientid asc;
            
         </li>
        <br>
      <li>this query links both the "invoices" and "clients" tables and will find clients with invoices greater than 1000
        </li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="instructorCode">
    <p>Instructor's SQL Code:</p>
    <ul>
      <li>an example of a join created by the instructor is: <br>
            select invoice_amount, invoice_description, invoices.clientid, clientfname ||' '|| clientlname
            as fullname, clientemail, clientphone  <br>
            from invoices 
            left join clients on clients.clientid = invoices.clientid <br>
            where invoice_amount >1000 or invoice_description like '%consultation%'<br>
            order by invoices.clientid desc;
            
         </li>
        <br>
      <li>this query does as mentioned above and also finds clients with "consultation" in their invoice description
        </li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="helpfulLinks">
    <p>Helpful SQL Links:</p>
     <ol>
        <li><a href="/www.w3schools.com/sql/sql_join.asp">www.w3schools.com</a></li>
        <br>
        <li><a href="/www.sql-join.com/sql-join-types">www.sql-join.com/sql-join-types</a></li>
    </ol>

</asp:Content>