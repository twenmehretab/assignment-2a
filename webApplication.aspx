﻿<%@ Page Language="C#" MasterPageFile="~/site.master" Inherits="assignment2a_N01294019.Default" %>

<asp:Content runat="server" ContentPlaceHolderID="Concept">   
    <p>A Web Application Concept:</p>
     <ul>
      <li>"asp.net checkbox"</li>
        <br>
      <li>my understanding of asp.net checkbox is that it is to be used when you want the 
        user to be able to select multiple options</li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="myCode">
   <p>My Web Application Code:</p>
    <ul>
       <li>
            <asp:Label ID="RoomService" runat="server">Room Service:</asp:Label> <br>
            <input id="service1" type="checkbox" name="roomservice" />
           <asp:Label ID="RoomService1" runat="server">Breakfast</asp:Label>
           <input id="service2" type="checkbox" name="roomservice" />
           <asp:Label ID="RoomService2" runat="server">Lunch</asp:Label>
           <input id="service3" type="checkbox" name="roomservice" /> 
            <asp:Label ID="RoomService3" runat="server">Dinner</asp:Label> 
        </li>
        <br>
       <li>this code allows users to select multiple options </li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="instructorCode">
    <p>Instructor's Web Application Code:</p>
    <ul>
       <li>
            <asp:Label ID="courses" runat="server">Select Courses:</asp:Label> <br>
            <input id="course1" type="checkbox" name="schoolCourse" />
           <asp:Label ID="courses1" runat="server">English</asp:Label>
           <input id="course2" type="checkbox" name="schoolCourse" />
           <asp:Label ID="courses2" runat="server">Science</asp:Label>
           <input id="course3" type="checkbox" name="schoolCourse" /> 
            <asp:Label ID="courses3" runat="server">Math</asp:Label> 
         
        </li>
        <br>
       <li>as mentioned above, this code allows users to select multiple options </li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="helpfulLinks">
    <p>Helpful Web Application Links:</p>
    <ol>
        <li><a href="/www.w3schools.com/howto/howto_css_custom_checkbox.asp">www.w3schools.com</a></li>
        <br>
        <li><a href="/www.guru99.com/asp-net-controls.html">www.guru99.com/asp-net-controls.html</a></li>
    </ol>
   
</asp:Content>