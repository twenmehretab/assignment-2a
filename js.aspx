﻿<%@ Page Language="C#" MasterPageFile="~/site.master" Inherits="assignment2a_N01294019.Default" %>

<asp:Content runat="server" ContentPlaceHolderID="Concept">   
    <p>A JavaScript Concept:</p>
    <ul>
      <li>"for loops"</li>
        <br>
      <li>my understanding of a for loop is that the loop will continue to run, 
          as long as the condition is true</li>
        <br>
      <li>therefore if the condition is false, it will stop running</li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="myCode">
   <p>My JavaScript Code:</p>
    <ul>
      <li>an example of a for loop I created is: <br>
         for (var i=0; i <= 9; i++){ <br> }</li>
        <br>
      <li>this code will run ten times (rather than typing it out manually) 
          and then will stop running once the condition becomes false</li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="instructorCode">
    <p>Instructor's JavaScript Code:</p>
    <ul>
       <li>an example of a for loop given by the instructor is: <br>
           for (var i=0; i < 10; i= i + 1){ <br> }
        </li>
        <br>
       <li>as explained by the instructor, this code will run 10 times, as long as "i" is less than 10, 
           and then it will increase the value by 1 after each loop</li>
        <br>
       <li>inside the curly brackets is the code that will run each time</li>
    </ul>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="helpfulLinks">
    <p>Helpful JavaScript Links:</p>
        
    <ol>
        <li><a href="/www.w3schools.com/js/js_loop_for.asp">www.w3schools.com</a></li>
        <br>
        <li><a href="/www.tutorialspoint.com/javascript/javascript_for_loop.htm">www.tutorialspoint.com</a></li>
    </ol>
        
</asp:Content>